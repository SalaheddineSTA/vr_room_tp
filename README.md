# Tp Réalité Virtuelle 
Simulateur examen IRM; les bases de la réalité virtuelle avec :
- Unity 2019.4.13f1 (LTS)
- Oculus Rift S

![](images/Picture1.png)
![](images/Picture2.png)

## Tâches
- [x] Mouvement continue
- [x] Manipulation (Prendre des objets)
- [x] Ajouter les composantes pour ouvrir une porte.
- [x] Ajouter un Script pour Scanner le patient 
- [x] Ajouter un Script pour afficher les image de l'IRM (Dicom en format PNG)

## Reste à faire
- [ ] Dans le script ToScan.cs, faire le mouvement inverse pour le patient et la table d'IRM.
- [ ] Dans le script ImageScroll.cs, afficher les images précédentes.

