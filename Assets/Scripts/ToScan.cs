﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
public class ToScan : MonoBehaviour
{
    public InputDeviceCharacteristics ControllerChar;
    private InputDevice input;
    private Vector3 startPositionPatient;
    private Vector3 endPositionPatient;

    private Vector3 startPositionMRI;
    private Vector3 endPositionMRI;
    public float speed = 2.0f;
    private bool isCollision = false;
    public ImageScroll imgscroll;
    // Start is called before the first frame update
    void Start()
    {

        List<InputDevice> devices = new List<InputDevice>();

        InputDevices.GetDevicesWithCharacteristics(ControllerChar,devices);

        if(devices.Count>0)
        {
            input = devices[0];
        }


        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("ToScan");

        if(gameObjects.Length>0)
        {
            foreach (GameObject go in gameObjects)
            {
                if(go.name == "character" )
                {
                    startPositionPatient = new Vector3(go.transform.position.x, go.transform.position.y, go.transform.position.z);
                    endPositionPatient = new Vector3(go.transform.position.x, go.transform.position.y,go.transform.position.z+1.5f);
                }

                if(go.name == "MRI_bed-top" )
                {
                    startPositionMRI = new Vector3(go.transform.position.x, go.transform.position.y,go.transform.position.z);
                    endPositionMRI = new Vector3(go.transform.position.x, go.transform.position.y,go.transform.position.z+1.5f);
                }
                    
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        input.TryGetFeatureValue(CommonUsages.primaryButton,out bool pBtnValue);

        if(pBtnValue && isCollision)
        {
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("ToScan");

            if(gameObjects.Length>0)
            {
                foreach (GameObject go in gameObjects)
                {
                    if(go.name == "character" )
                    {
                        go.transform.position = Vector3.MoveTowards(go.transform.position, endPositionPatient, Time.deltaTime * speed);
                    }

                    if(go.name == "MRI_bed-top" )
                    {
                        go.transform.position = Vector3.MoveTowards(go.transform.position, endPositionMRI, Time.deltaTime * speed);
                    }
                        
                }
            }
            imgscroll.setScanned(true);
        }

        //Challenge
        //Refaire sortir le patient et la table 
        //Secondary button
    }

    void OnTriggerEnter(Collider c)
    {
        if(c.CompareTag("Hand"))
        {
            isCollision = true;
        }
    }

    void OnTriggerExit(Collider c)
    {
        if(c.CompareTag("Hand"))
        {
            isCollision = false;
        }
    }
}
